var path = require('path')
var webpack = require('webpack')

module.exports = {
  entry: './src/index.js',
  output: {
      path: path.resolve(__dirname, 'www/js'),
      filename: "bundle.js"
  },
  module: {
    loaders: [
      {
        test: /\.vue$/,
        loader: 'vue-loader'
      },
      {
        test: /\.css$/,
        loader: 'style!css'
      },
      {
        test: /\.js$/,
        loader: 'babel-loader',
        exclude: /node_modules/,
        query: {compact: false}
      },
      {
        test: /\.(png|jpg|gif|svg)$/,
        loader: 'file-loader',
        options: {
          name: '[name].[ext]?[hash]'
        }
      }
    ]
  }/*,
  resolve: {
    alias: {
      'vue$': 'vue/dist/vue.common.js',
      '$': './frontend/static/jquery-3.1.1.min.js'
    }
  }*/
};
