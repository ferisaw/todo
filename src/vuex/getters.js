import * as types from '../constants/mutations.js'

export default {
  currentGroup: (state, getters) => {
    if (state.openedGroupId) {
      var tasks = state.groups[state.openedGroupId].tasks.map((taskId) => state.tasks[taskId]);
      return { ...state.groups[state.openedGroupId], tasks };
    }
  }
}
