import Vue from 'vue'
import {normalize, Schema, arrayOf} from 'normalizr';

import {
  CHANGE_PAGE,
  OPEN_GROUP,
  CLOSE_GROUP,
  ADD_GROUP,
  EDIT_GROUP,
  ADD_TASK,
  UPDATE_TASK,
  LOAD_GROUPS,
  DELETE_GROUP,
  DELETE_TASK
} from '../constants/mutations.js'
import {
  INDEX_PAGE,
  GROUP_PAGE
} from '../constants/common.js'

import { Group } from '../models/group.js'
import { Task } from '../models/task.js'
import '../models/index.js'

const groupSchema = new Schema('groups');
const taskSchema = new Schema('tasks');
groupSchema.define({
  tasks: arrayOf(taskSchema)
});
taskSchema.define({
  group: groupSchema
});

export default {
// var newGroup = new Group({ title: 'SUPER NEW GROUP', description: 'test description' });
// var newTask = new Task({ text: 'SUPER NEW TASK', isDone: false });
// persistence.add(newGroup);
// persistence.add(newTask);
// newGroup.tasks.add(newTask);
// Task.all().prefetch('group')
// persistence.flush(() => {});

  [LOAD_GROUPS] (state) {
    Group.all().list(null, function (results) {
      results.forEach(group => {
        group.tasks.selectJSON(['*'], tasks => {
          let outputGroup = {
            id: group.id,
            title: group.title,
            description: group.description,
            tasks
          };

          var normalized = normalize(outputGroup, groupSchema);
          state.groups = { ...state.groups, ...normalized.entities.groups };
          state.tasks = { ...state.tasks, ...normalized.entities.tasks };
        });
      });
    });
  },
  [OPEN_GROUP] (state, id) {
    state.openedGroupId = id;
    state.currentPage = GROUP_PAGE;
  },
  [CLOSE_GROUP] (state) {
    state.openedGroupId = null;
    state.currentPage = INDEX_PAGE;
  },
  [ADD_GROUP] (state, newGroup) {
    var persistenceGroup = new Group({
      title: newGroup.title,
      description: newGroup.description4
    });
    persistence.add(persistenceGroup);
    newGroup.id = persistenceGroup.id;
    persistence.flush(() => {
      var normalized = normalize(newGroup, groupSchema);
      Vue.set(state.groups, normalized.result, normalized.entities.groups[normalized.result]);
    });
  },
  [EDIT_GROUP] (state, newGroup) {
    Group.load(newGroup.id, group => {
      group.title = newGroup.title || group.title;
      group.description = newGroup.description || group.description;
      persistence.flush(() => {
        var normalized = normalize(newGroup, groupSchema);
        state.groups[newGroup.id] = { ...state.groups[newGroup.id], ...normalized.entities.groups[newGroup.id] };
      });
    });
  },
  [DELETE_GROUP] (state, groupId) {
    Group.load(groupId, group => {
      let deletedTasks = [];
      group.tasks.list(tasks => {
        tasks.forEach(task => {
          deletedTasks.push(task.id);
          persistence.remove(task);
        });
      })
      persistence.remove(group);
      persistence.flush(() => {
        Vue.delete( state.groups, groupId );
        delete deletedTasks.forEach(taskId => delete state.tasks[taskId]);
      });
    });
  },
  [ADD_TASK] (state, newTask) {
    var task = new Task({
      text: newTask.text,
      isDone: false,
      group: newTask.group
    });
    persistence.add(task);
    Group.load(newTask.group, (group) => {
      group.tasks.add(task);
    });
    persistence.flush(() => {
      task.selectJSON(['*'], task => {
        var normalized = normalize(task, taskSchema);
        state.tasks = { ...state.tasks, ...normalized.entities.tasks };
        state.groups = { ...state.groups,
                         [newTask.group]: {
                           ...state.groups[newTask.group],
                           tasks: [ ...state.groups[newTask.group].tasks, normalized.result ]
                         }
                       };
      });
    })
  },
  [UPDATE_TASK] (state, updatedTask) {
    Task.load(updatedTask.id, task => {
      task.text = updatedTask.text || task.text;
      if ('isDone' in updatedTask) task.isDone = updatedTask.isDone;
      persistence.flush(() => {
        var normalized = normalize(updatedTask, taskSchema);
        if (state.tasks[updatedTask.id])
          state.tasks[updatedTask.id] = { ...state.tasks[updatedTask.id], ...normalized.entities.tasks[updatedTask.id] };
      })
    });
  },
  [DELETE_TASK] (state, taskId) {
    let groupId;
    Task.load(taskId, task => {
      groupId = task.group.id;
      persistence.remove(task);
    });
    persistence.flush(() => {
      Vue.delete( state.tasks, taskId );
      let index = state.groups[groupId].tasks.indexOf(taskId);
      if (index !== -1)
        state.groups[groupId].tasks.splice(index, 1);
    })
  }
}
