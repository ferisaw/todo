import * as types from '../constants/mutations.js'

export default {
  loadGroups ({ commit }) {
    commit(types.LOAD_GROUPS);
  },
  openGroup ({ commit }, id) {
    commit(types.OPEN_GROUP, id);
  },
  closeGroup ({ commit }) {
    commit(types.CLOSE_GROUP);
  },
  addGroup ({ commit }, newGroup) {
    commit(types.ADD_GROUP, newGroup);
  },
  editGroup ({ commit }, newGroup) {
    commit(types.EDIT_GROUP, newGroup);
  },
  deleteGroup ({ commit }, groupId) {
    commit(types.DELETE_GROUP, groupId);
  },
  addTask({ commit }, newTask) {
    commit(types.ADD_TASK, newTask);
  },
  updateTask({ commit }, updatedTask) {
    commit(types.UPDATE_TASK, updatedTask);
  },
  deleteTask({ commit }, taskId) {
    commit(types.DELETE_TASK, taskId);
  }
}
