import IndexPage from '../vue/index-page.vue'
import GroupPage from '../vue/group-page.vue'
import CreateGroup from '../vue/create-group.vue'
import CreateTask from '../vue/create-task.vue'

export default [
  { path: '/', component: IndexPage },
  { path: '/group', component: GroupPage },
  { path: '/creategroup', component: CreateGroup },
  { path: '/createtask', component: CreateTask }
]
