import Vue from 'vue'
import VueRouter from 'vue-router'
import VueTouch from 'vue-touch'
import App from './vue/app.vue'
import store from './vuex/store.js'

import { Group } from './models/group.js'
import { Task } from './models/task.js'
import './models/index.js'
import routes from './router'

persistence.store.cordovasql.config(
  persistence,
  'todo',
  '0.0.1',                // DB version
  'Todo database',        // DB display name
  5 * 1024 * 1024,        // DB size (WebSQL fallback only)
  0,                      // SQLitePlugin Background processing disabled
  2                       // DB location (iOS only), 0 (default): Documents, 1: Library, 2: Library/LocalDatabase
);

/*persistence.defineMigration(1, {
  up: function() {
    console.log('UPDATING DB');
    this.executeSql('DROP TABLE Task;');
  }
});

persistence.migrations.init(function() {

  console.log('got here');
  persistence.migrate( function(){
    console.log('migration complete!');
  } );
});*/
if (process.env.NODE_ENV === 'development')
  persistence.debug = true;
persistence.schemaSync();



/*var newGroup = new Group({ title: 'SUPER NEW GROUP', description: 'test description' });
var newTask = new Task({ text: 'SUPER NEW TASK', isDone: false });
persistence.add(newGroup);
persistence.add(newTask);
newGroup.tasks.add(newTask);
Task.all().prefetch('group')*/
/*persistence.flush(() => {
  Group.all().list(null, function (results) {
    results.forEach(result => {
      persistence.remove(result);
    })
  })
  Task.all().list(null, function (results) {
    results.forEach(result => {
      persistence.remove(result);
    })
  });
  /*Task.all().list(null, function (results) {
    results.forEach(result => {
      console.log('tasks:', result);
    })
  });
});*/

//console.log('all group:', Group.all());

window.addEventListener('load', function () {
  Vue.use(VueTouch, {name: 'v-touch'});
  Vue.use(VueRouter);

  new Vue({
    el: '#app',
    store,
    router: new VueRouter({ routes }),
    render: h => h(App)
  });
})
