export const INDEX_PAGE = 'index'
export const GROUP_PAGE = 'group'

export const GROUP_TEMPLATE = {
  title: '',
  description: '',
  tasks: []
};
export const TASK_TEMPLATE = {
  text: '',
  isDone: false
}
