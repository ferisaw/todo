import { Group } from './group.js'
import { Task } from './task.js'

//module.exports.init =
Group.hasMany('tasks', Task, 'group');
Task.hasOne('group', Group);
